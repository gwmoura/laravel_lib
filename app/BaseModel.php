<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
	public $timestamps = FALSE;
		
	// 
	public function getLabel($attribute) {
		return $this->label[$attribute];
	}
	
	// 
	public function getAllLabel() {
		$label = [];
		foreach ($this->label as $key => $value)
			$label[ $this->getClassName() . '.' . $key] = $value;
		
		return $label;
	}
	
	// 
	public function getClassName(){
		return substr(get_called_class(), strrpos(get_called_class(), '\\') + 1);
	}
}