<?php
namespace App\Grid;

/*
A FAZER (por prioridade)

 - add checkbox, com opcao de selecionar todos
 - add opcoes para alterar configuracoes da js DataTable (grid.js);
 - processar os dados da paginacao sob demanda e nao carregar tudo de vez no html;
 - add download no formato PDF, Excel, CSV (deve ser feito de todo o conteudo, ex: tem 10 paginas, o conteudo das 10 paginas deve ser fazer parte do download);
 - add search autocomplete 
 
*/
// 
class Grid {
	
	public $id;
	public $config;	
	public $label;
	public $data;
	public $ouput = NULL;
	
	// 
	public function __construct($config = []) {
		// Grid - config
		$configDefault = array(
			'htmlTable' => array(
				'id' => 'example',
				'class' => 'table table-striped table-bordered',
				'cellspacing' => '0',				
				'width' => '100%',
			)
		);
		
		// O array do "usuario" tem mais prioridade que a configDefault.		
		$this->config = $this->array_merge_recursive_ex($configDefault, $config);
		$this->id = $this->config['htmlTable']['id'];
		
		if (!isset($this->config['htmlTable'])) $this->config['htmlTable'] = [];
		if (!isset($this->config['htmlThead'])) $this->config['htmlThead'] = [];
		if (!isset($this->config['htmlTrTitle'])) $this->config['htmlTrTitle'] = [];
		if (!isset($this->config['htmlThTitle'])) $this->config['htmlThTitle'] = [];
		
		if (!isset($this->config['htmlTrData'])) $this->config['htmlTrData'] = [];
		if (!isset($this->config['htmlTdData'])) $this->config['htmlTdData'] = [];		
		
		$this->label = $config['label'];
		//$this->data =  $config['data'];
		$this->ouput = NULL;
	}
	
	// 
	public function init() {		
		$this->ouput .= $this->makeHtmlGrid();
		//$this->ouput .= $this->makeJS();
		
		return $this->ouput;
	}
	
	// 
	public function obfuscator($data) {
		
	}
	
	// 
	public function makeHtmlGrid() {
		$this->message();
		$this->buttons();
		
		// Grid - begin
		$ouput = "
			<table ". $this->makeHTMLTags($this->config['htmlTable']) .">
				<thead ". $this->makeHTMLTags($this->config['htmlThead']) .">
					<tr ". $this->makeHTMLTags($this->config['htmlTrTitle']) .">
		";
		
		// select all
		$ouput .= "<th> <input type='checkbox' id='checkAll' name='checkAll' value=''> </th>";
		
		// Grid title fields
		foreach($this->label as $value) {			
			$ouput .= "<th ". $this->makeHTMLTags($this->config['htmlThTitle']) .">$value</th>";
		}
		
		$ouput .= "</tr>
				</thead>						
			";
		
		// Grid - data
		//$ouput .= $this->makeHtmlData();
		
		// Grid - end
		$ouput .= "</table>";		
		
		return $ouput;
	}
	
	// 
	public function makeHtmlData() {
		$ouput = "<tbody>";
		
		foreach($this->data as $key => $value) {
			$ouput .= "<tr ". $this->makeHTMLTags($this->config['htmlTrData']) .">";					
			
			$column = 1;
			foreach($this->label as $key_2 => $value_2) {												
				if ($column == 1)
					$ouput .= "<td> <input type='checkbox' name='id[]' value='". $value['id'] ."'> </td>";		
				
				$ouput .= "<td ". $this->makeHTMLTags($this->config['htmlTdData']) .">$value[$key_2]</td>";
				$column++;
			}
			
			$ouput .= "</tr>
					</tbody>
				";
		}
			
		return $ouput;
	}
	
	// 
	public function buttons() {
		$this->ouput .= "
			<form method='POST' action='documento/destroyAll' id='formDelete'>
				 ". csrf_field() ."
				<input type='hidden' name='_method' value='DELETE'>	
			</form>		
			
			<button type='button' class='btn btn-primary' id='create' title='Cadastrar'>
				<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>
			</button>

			<button type='button' class='btn btn-primary' id='edit' title='Alterar'>
				<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span>
			</button>

			<button type='button' class='btn btn-primary' id='delete' title='Excluir'>
				<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
			</button>

			<button type='button' class='btn btn-primary' id='view' title='Visualizar'>
				<span class='glyphicon glyphicon-search' aria-hidden='true'></span>
			</button>

			<button type='button' class='btn btn-primary' id='refresh' title='Atualizar'>
				<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>
			</button>

			<button type='button' class='btn btn-primary' id='download' title='Download'>
				<span class='glyphicon glyphicon-circle-arrow-down' aria-hidden='true'></span>
			</button>
			";	

		$this->ouput .= $this->menu( $this->config );
		
		// temp
		$this->ouput .= "<br><br>";
	}
	
	public function menu() {
		$output = NULL;
		
		if (isset($this->config['menu'])) {
			$menu = $this->config['menu'];		
		
			$output .= "
				<!-- Button dropdown -->
				<div class='btn-group'>
					<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
						Mais <span class='caret'></span>
					</button>
					<ul class='dropdown-menu'>
				";
				
			foreach($menu as $value) {
				$output .= "<li><a href='". $value['href'] ."'>". $value['label'] ."</a></li>";
			}
			
			$output .= "	
					</ul>
				</div>			
			";
		}
		
		return $output;
	}
	
	// 
	public function message() {
		if (session('response'))
			$this->ouput .= "
				<div class='alert alert-". session('response')['type'] ."'>
					". session('response')['message'] ."
				</div>
			";
	}
	
	// 
	public function makeHTMLTags($data) {
		$ouput = NULL;
		
		foreach($data as $key => $value) {				
			$ouput .= $key . "='" . $value . "' ";
		}
		
		return $ouput;
	}	
	
	// Make JS
	public function makeJS() {
		$ouput = "
				<script>
					grid('". $this->id ."');
				</script>
		";
		
		return $ouput;
	}
	
	// by http://stackoverflow.com/questions/25712099/php-multidimensional-array-merge-recursive?answertab=active#tab-top
	function array_merge_recursive_ex(array & $array1, array & $array2) {
		$merged = $array1;

		foreach ($array2 as $key => & $value) {
			if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
				$merged[$key] = $this->array_merge_recursive_ex($merged[$key], $value);
			}
			else 
				if (is_numeric($key)) {
					if (!in_array($value, $merged))
						$merged[] = $value;
				} 
				else
					$merged[$key] = $value;
		}

		return $merged;
	}	
}
?>