<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
//use App\Http\Requests;
//use Validator;

//
class BaseController extends Controller {

	//
	/*
	function __construct() {
		parent::__construct();
   }
   */

	//
	public function showErros($errors) {
		$output = NULL;

		if (count($errors) > 0) {
			$output .= "
				<div class='alert alert-danger'>
					<ul>
				";

			foreach ($errors->all() as $error)
				$output .= "<li> $error </li>";

			$output .= "
					</ul>
				</div>
				";
		}

		return $output;
	}

	//
	public function validateMessages() {
		return [
			'required'  => 'Campo :attribute é obrigatório.',
		];
	}

	// Rertun the current action name of the Controller
	public function getControllerActionName() {
		$result = explode('@', Route::getCurrentRoute()->getActionName());

		return $result[1];
	}

	// Rertun the current name of the Controller
	public function getControllerUri() {
		$controllerUri = explode('/', Route::getCurrentRoute()->uri());

		return $controllerUri[0];
	}





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		$this->title = 'Gerenciar ' . $this->title;
		$model =  new $this->modelPath();

		return view( $this->getControllerUri() . '.index', ['model' => $model, 'controller' => $this]);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$this->title = 'Cadastrar ' . $this->title;

		$model = new $this->modelPath();
        return view( $this->getControllerUri() . '.create', ['model' => $model, 'controller' => $this]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$modelName = $this->modelName;

		$model = new $this->modelPath();
		$model->fill( $request->$modelName );
		$this->validate($request, $model->getRules(), $this->validateMessages(), $model->getAllLabel());
		$model->save();
        
        return Redirect::route( $this->getControllerUri() . '.index')->with( 'response', ['type' => 'success', 'message' => 'Cadastrado com sucesso!'] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$this->title = 'Visualizar ' . $this->title;
		
		$modelPath = $this->modelPath;

        $model = $modelPath::find($id);
		return view( $this->getControllerUri() .'.show', ['model' => $model, 'controller' => $this]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->title = 'Alterar ' . $this->title;
        
		$modelPath = $this->modelPath;

		$model = $modelPath::find($id);
        return view( $this->getControllerUri() . '.edit', ['model' => $model, 'controller' => $this]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$modelPath = $this->modelPath;
		$modelName = $this->modelName;

		$model = $modelPath::find($id);
		$model->fill( $request->$modelName );
		$this->validate($request, $model->getRules(), $this->validateMessages(), $model->getAllLabel());
		$model->save();
        
        return Redirect::route( $this->getControllerUri() . '.index')->with( 'response', ['type' => 'success', 'message' => 'Alterado com sucesso!'] );
    }

    /**
     * Remove the selected resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAll()
    {
		$modelPath = $this->modelPath;
		foreach($_POST['ids'] as $id) {
			$model = $modelPath::find($id);
			$model->delete();
		}

		return Redirect::route( $this->getControllerUri() . '.index')->with( 'response', ['type' => 'success', 'message' => 'Excluído com sucesso!'] );
    }
}
?>