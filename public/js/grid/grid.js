// 
function grid(config) {
    $(document).ready(function() {
		// Valor padrao do order (column and type)
		if (!config.hasOwnProperty("order")) {
			config.order = { 
				'column': 1,
				'type': 'asc'
			}
		}
		else {
			if (!config.order.hasOwnProperty("column"))
				config.order.column = 1 ;
			if (!config.order.hasOwnProperty("type"))
				config.order.type = 'asc' ;
		}
		
        $('#' + config.id ).DataTable({
            "language": {
                "url": "/js/dataTables/dataTables.pt-br.json"
            },
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "Todos"]
            ],
		
			"columnDefs": [
				{
					// The `data` parameter refers to the data for the cell (defined by the
					// `data` option, which defaults to the column being worked with, in
					// this case `data: 0`.
					"render": function ( data, type, row ) {
						return "<input type='checkbox' name='id[]' value='"+ data +"'>";
					},
					"orderable": false,
					//"visible": false,
					//"defaultContent": "-",
					"width": "20px",
					"targets": 0
				},            
			],

			"columns": config.columns,

			// Ajax
			"processing": true,
			"serverSide": true,
			"ajax": config.ajaxUrl,
			
            "order": [
                [config.order.column, config.order.type]
            ]
        });
		
		//console.log(config.baseUrl);
		
		checkAll(config.id);
		create(config);
		edit(config);
		destroy(config);
		view(config);
		refresh();
    });
}

// 
function checkAll(id) {
	$('#checkAll').change(function(){
		if ($('#checkAll').is( ":checked" ))
			$('#' + id + ' td input:checkbox').prop( "checked", true );
	else
		$('#' + id + ' td input:checkbox').prop( "checked", false );
	})	
}

// 
function getCheckedAllValues(id) {
	ids = $('#' + id + ' td input:checkbox:checked').map(function() {
		return this.value;
	}).get();
	
	return ids;
}

// 
function create(config) {
	$('#create').click(function(){
		$( this ).attr('disabled',true);
		window.location.href = config.baseUrl + '/create';
	});	
}

// 
function edit(config) {
	$('#edit').attr('disabled',true);
	
	$( 'body' ).on( "change", '#' + config.id + ' input:checkbox', function(){
		if ($('#' + config.id + ' input:checkbox:checked').length == 1) {
			$('#edit').attr('disabled',false);
		}
		else
			$('#edit').attr('disabled',true);
	});
	
	$('#edit').click(function(){
		$( this ).attr('disabled',true);
		
		id = $('#' + config.id + ' input:checkbox:checked').val();
		window.location.href = config.baseUrl + '/' + id + '/edit';
	});	
}

// 
function destroy(config) {
	$('#delete').attr('disabled',true);		
	
	$( 'body' ).on( "change", '#' + config.id + ' input:checkbox', function(){		
		if ($( '#' + config.id + ' input:checkbox:checked').length > 0) {
			$('#delete').attr('disabled',false);
		}
		else
			$('#delete').attr('disabled',true);
	});
	
	$('#delete').click(function(){
		$( this ).attr('disabled',true);
		
		ids = getCheckedAllValues(config.id);
		
		if (confirm('Tem certeza que deseja excluir o(s) ' + ids.length + ' registro(s)?')) {
			$("#formDelete").append("");
			ids.forEach(function(value, index) {
				$("#formDelete").append("<input type='hidden' name='ids[]' value='" + value + "'>");					
			});				
			$("#formDelete").submit();
		}
	});	
}


// 
function view(config) {
	$('#view').attr('disabled',true);
	
	$( 'body' ).on( "change", '#' + config.id + ' input:checkbox', function(){
		if ($('#' + config.id + ' input:checkbox:checked').length == 1) {
			$('#view').attr('disabled',false);
		}
		else
			$('#view').attr('disabled',true);
	});
	
	$('#view').click(function(){
		$( this ).attr('disabled',true);
		
		id = $('#' + config.id + ' input:checkbox:checked').val();
		window.location.href = config.baseUrl + '/' + id;
	});	
}

// 
function refresh() {
	$('#refresh').click(function(){
		location.reload();
	});
}