<?php
use App\View\View;

$view = new View( ['controller' => $controller, 'errors' => $errors] );
?>

@extends('layouts.app')

@section('content')	
	<? $view->getHeader(); ?>
	
	<?= Form::formBegin( ['action' => route('documento.store')] ) ?>
		{{ csrf_field() }}
		
		<?= Form::htmlInput( ['type' => 'money', 'name' => 'Documento[descricao]', 'value' => old('Documento.descricao'), 'htmlOptions' => ['id' => 'documento-descricao'], 'label' => ['content' => $model->getLabel('descricao')], 'errors' => ($errors->has('Documento.descricao')) ] ) ?>
		<?= Form::htmlInput( ['type' => 'text', 'name' => 'Documento[sigla]', 'label' => ['content' => $model->getLabel('sigla')], 'errors' => ($errors->has('Documento.sigla')) ] ) ?>
		
        <?= Form::formButtons( ['baseUrl' => $controller->getControllerUri()] ) ?>
	<?= Form::formEnd() ?>	
@endsection

@section('js')
	$( document ).ready(function() {
		<?= Form::$outputJs; ?>
	});
@endsection	