<?php
use App\View\View;

$view = new View( ['controller' => $controller, 'errors' => $errors] );
?>

@extends('layouts.app')

@section('content')
	<? $view->getHeader(); ?>
	
	<?= Form::formBegin( ['action' => route( $controller->getControllerUri() . '.update', ['id'=> $model->id_documento])] ) ?>
		{{ csrf_field() }}
		<input type='hidden' name='_method' value='PUT'>        
        
		<?= Form::htmlInput( ['type' => 'text', 'name' => 'Documento[descricao]', 'value' => old('Documento.descricao', $model->descricao), 'htmlOptions' => ['id' => 'documento-descricao'], 'label' => ['content' => $model->getLabel('descricao')], 'errors' => ($errors->has('Documento.descricao')) ] ) ?>
		<?= Form::htmlInput( ['type' => 'text', 'name' => 'Documento[sigla]', 'value' => old('Documento.sigla', $model->sigla), 'htmlOptions' => ['id' => 'documento-sigla'], 'label' => ['content' => $model->getLabel('sigla')], 'errors' => ($errors->has('Documento.sigla')) ] ) ?>
        
		<?= Form::formButtons( ['baseUrl' => $controller->getControllerUri()] ) ?>
	<?= Form::formEnd() ?>	
@endsection

@section('js')
	$( document ).ready(function() {
		<?= Form::$outputJs; ?>
	});
@endsection	