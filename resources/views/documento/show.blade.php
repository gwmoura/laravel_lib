<?php
use App\View\View;

$view = new View( ['controller' => $controller, 'errors' => $errors] );
?>

@extends('layouts.app')

@section('content')

	<? $view->getHeader(); ?>

	<table class="table table-striped">
		<?
		foreach($model->label as $key => $value) {
			echo "
				<tr>
					<td width='30%'>". $model->getLabel($key) ."</td>
					<td width='70%'>". $model->$key ."</td>
				</tr>
			";
		}		
		?>
	</table>

	<?= Form::backButton( ['baseUrl' => $controller->getControllerUri()] ) ?>
@endsection

@section('js')
	$( document ).ready(function() {
		<?= Form::$outputJs; ?>
	});
@endsection	