<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/css/app.css">
    
	<!-- SmartMenus jQuery CSS -->
    <link rel="stylesheet" href="/css/jquery.smartmenus.bootstrap.css">
	
    <!-- dataTables CSS -->
	<link rel="stylesheet" href="/css/dataTables/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="/css/dataTables/font-awesome.min.css">
	
	  <!-- jquery UI -->
	<link rel="stylesheet" href="/css/jquery-ui/jquery-ui.css">
	  
	<link rel="stylesheet" href="/css/grid/grid.css">
	<link rel="stylesheet" href="/css/crud/crud.css">
	
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
	<!-- Menu -->
	<!-- Navbar fixed top -->
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Mathitís</a>
			</div>
			<div class="navbar-collapse collapse">
				<!-- Left nav -->
				<ul class="nav navbar-nav">
					<li><a href="#">Início</a></li>
					<li><a href="#">Edital <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Gerenciar</a></li>
							<li><a href="#">Escolas</a></li>
							<li><a href="#">Faixas de Remuneração</a></li>
							<li><a href="#">Documentos</a></li>
							<li><a href="#">Candidatos</a></li>
						</ul>
						<li><a href="#">Configuração</a></li>
						<li><a href="#">Usuário</a></li>
						<li><a href="#">Relatório / Consultas</a></li>
					</li>
				</ul>
	  
				<!-- Right nav -->
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">Fulano <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Minha conta</a></li>
							<li><a href="#">Alterar senha</a></li>
							<li class="divider"></li>
							<li><a href="#">Sair</a></li>
						</ul>
					</li>
				</ul>	  
			</div><!--/.nav-collapse -->
		</div><!--/.container -->
	</div>
	<!-- END Menu -->

	
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

	<div style='width: 100%;'>
		<div style='margin: 0 auto; width: 80%;'>
			@yield('content')
		</div>
	</div>
	
    <!-- Scripts -->
    <script src="/js/app.js"></script>    
    
	<!-- SmartMenus jQuery Scripts -->
	<script src="/js/jquery.smartmenus.min.js"></script>
    <script src="/js/jquery.smartmenus.bootstrap.min.js"></script>
	
	<!-- dataTables Scripts -->
	<script src="/js/dataTables/jquery.dataTables.min.js"></script>
	<script src="/js/dataTables/dataTables.bootstrap.min.js"></script>
	
	<!-- jQuery masked input -->
	<script src="/js/jquery.maskedinput/jquery.maskedinput.js" type="text/javascript"></script>
	
	<!-- jQuery UI -->
	<script src="/js/jquery-ui/jquery-ui.js"></script>
	
	<!-- jQuery maskMoney -->
	<script src="/js/jquery.maskMoney/jquery.maskMoney.min.js"></script>
	
	<script src="/js/grid/grid.js"></script>
	<script src="/js/crud/crud.js"></script>
	
	<script>
		@yield('js');
	</script>	
</body>
</html>
